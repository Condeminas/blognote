var express = require("express"),
    handlebars = require('express-handlebars');
 
var bodyParser = require('body-parser');
 
var app = express();

 
var posts = [{
                "subject": "Post numero 1",
                "description": "Descripcion 1",
                "time": new Date()
            },
            {
                "subject": "Post numero 2",
                "description": "Descripcion 2",
                "time": new Date()
            }
            ];

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())
           
           
app.engine('handlebars', handlebars());
app.set('view engine', 'handlebars');
app.set("views", "./views");
 
 //GET /posts
app.get('/posts', function(req, res){
    res.render('posts', { "title": "Hola gente", "posts" : posts } );
});
//GET /posts/new
app.get('/posts/new', function(req, res){
	console.log(posts.length);
    res.render('posts', { "title": "Lo ultimo", "posts" : [posts[(posts.length)-1]] } );
});
 //POST /posts
app.post('/posts', function(req, res){ 
    posts.push(req.body); 
    res.end(JSON.stringify(req.body));
});
//GET /posts/:id
app.get('/posts/:id', function(req, res){ 
	var id = req.params.id;
    posts.push(req.body{id}); 
	res.render('the post number ', id, { "title": "The requested", "posts" : [posts[id]] } );
});
//GET /posts/edit
app.get('/posts/edit', function(req, res){ 
    posts.push(req.body); 
    res.end(JSON.stringify(req.body));
});
//- PUT /posts/:id
app.put('/posts/:id', function(req, res){ 
	var id = req.params.id;
    posts{id}.push(req.body);    
	res.render('Nuevo post'); 
	res.end(JSON.stringify(req.body));
});
//DEL /posts/:id
app.delete('/posts/:id', function(req, res){ 
	var id = req.params.id;
    posts.splice(req.body[id]); 
    res.end(JSON.stringify(req.body));
});
 
app.listen(8080);